CREATE TABLE contact (
    id bigint NOT NULL,
    send_from text,
    send_to text,
    date_created timestamp without time zone,
    subject text,
    message text,
    status character varying(255)
);

CREATE SEQUENCE contact_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);
