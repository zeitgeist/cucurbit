CREATE TABLE ingredient (
    id bigint NOT NULL,
    description character varying(255),
    image_link character varying(255),
    last_updated timestamp without time zone,
    name character varying(255),
    status character varying(255)
);

CREATE TABLE ingredient_ratio (
    id bigint NOT NULL,
    last_updated timestamp without time zone,
    quantity bigint,
    status character varying(255),
    ingredient_id bigint
);

CREATE TABLE recipe (
    id bigint NOT NULL,
    description character varying(255),
    image_link character varying(255),
    last_updated timestamp without time zone,
    name character varying(255),
    status character varying(255)
);

CREATE TABLE recipe_ingredient_ratio (
    recipe_id bigint NOT NULL,
    ingredients_id bigint NOT NULL
);

CREATE SEQUENCE recipe_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE ingredient_ratio_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE ingredient_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY ingredient
    ADD CONSTRAINT ingredient_pkey PRIMARY KEY (id);

ALTER TABLE ONLY ingredient_ratio
    ADD CONSTRAINT ingredient_ratio_pkey PRIMARY KEY (id);
    
ALTER TABLE ONLY recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (id);
    
ALTER TABLE ONLY recipe_ingredient_ratio
    ADD CONSTRAINT uk_gaayk7ha243xnvngbf9ba4219 UNIQUE (ingredients_id);
    
ALTER TABLE ONLY recipe_ingredient_ratio
    ADD CONSTRAINT fk4wr0hri0ro02pttx9ittrsvsu FOREIGN KEY (ingredients_id) REFERENCES ingredient_ratio(id);
    
ALTER TABLE ONLY ingredient_ratio
    ADD CONSTRAINT fkf4y97f5c5bsiyn4syq1e6hs55 FOREIGN KEY (ingredient_id) REFERENCES ingredient(id);
    
ALTER TABLE ONLY recipe_ingredient_ratio
    ADD CONSTRAINT fkhixp6r4h9g8gsyjkxv9xywulx FOREIGN KEY (recipe_id) REFERENCES recipe(id);
