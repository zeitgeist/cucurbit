package com.zeitgeist.cucurbit.contact;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ContactRequestBody {

    private String sendFrom;
    private String subject;
    private String message;

}
