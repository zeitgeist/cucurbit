package com.zeitgeist.cucurbit.contact;

public enum ContactStatus {
    SENT,
    PENDING
}
