package com.zeitgeist.cucurbit.contact;

import com.zeitgeist.cucurbit.common.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ContactService {

    private final DateUtils dateUtils;
    private final ContactRepository contactRepository;
    private final String CUCURBIT_EMAIL_NOTIFICATION_RECIPIENT;

    public ContactService(DateUtils dateUtils,
                          ContactRepository contactRepository,
                          String CUCURBIT_EMAIL_NOTIFICATION_RECIPIENT) {
        this.dateUtils = dateUtils;
        this.contactRepository = contactRepository;
        this.CUCURBIT_EMAIL_NOTIFICATION_RECIPIENT = CUCURBIT_EMAIL_NOTIFICATION_RECIPIENT;
    }

    public Contact createAndSaveContact(ContactRequestBody requestBody) {
        Contact contact = new Contact();
        contact.setSendFrom(requestBody.getSendFrom());
        contact.setSendTo(CUCURBIT_EMAIL_NOTIFICATION_RECIPIENT);
        contact.setDateCreated(dateUtils.getCurrentDate());
        contact.setSubject(requestBody.getSubject());
        contact.setMessage(requestBody.getMessage());
        contact.setStatus(ContactStatus.PENDING);

        return contactRepository.save(contact);
    }

    public void updateContactStatus(Contact contact, ContactStatus status) {
        contact.setStatus(status);
        contactRepository.save(contact);
    }

}
