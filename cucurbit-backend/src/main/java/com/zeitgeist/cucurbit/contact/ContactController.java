package com.zeitgeist.cucurbit.contact;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class ContactController {

    private final ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(path = "api/v1/contact")
    public void contact(@RequestBody ContactRequestBody requestBody) {
        contactService.createAndSaveContact(requestBody);
    }

}
