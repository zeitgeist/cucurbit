package com.zeitgeist.cucurbit.contact;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long>  {

    List<Contact> findAllByStatus(ContactStatus status);

}
