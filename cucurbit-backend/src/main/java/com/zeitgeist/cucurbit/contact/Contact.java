package com.zeitgeist.cucurbit.contact;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
public class Contact {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_sequence")
    @SequenceGenerator(name = "contact_sequence", sequenceName = "contact_sequence", allocationSize = 1)
    private Long id;

    private String sendFrom;
    private String sendTo;
    private LocalDateTime dateCreated;
    private String subject;
    private String message;

    @Enumerated(EnumType.STRING)
    private ContactStatus status;

    public String getFormattedMessage() {
        return "<strong>FROM: " + this.sendFrom + "</strong> <br/><br/>" + this.message;
    }

}
