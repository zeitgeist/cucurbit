package com.zeitgeist.cucurbit.email;

import com.zeitgeist.cucurbit.contact.Contact;
import com.zeitgeist.cucurbit.contact.ContactRepository;
import com.zeitgeist.cucurbit.contact.ContactService;
import com.zeitgeist.cucurbit.contact.ContactStatus;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.List;

@Log4j2
@Component
public class EmailSenderJob {

    private final static String HOURLY = "0 0 * * * *";

    private final ContactRepository contactRepository;
    private final EmailService emailService;
    private final ContactService contactService;

    public EmailSenderJob(ContactRepository contactRepository, EmailService emailService, ContactService contactService) {
        this.contactRepository = contactRepository;
        this.emailService = emailService;
        this.contactService = contactService;
    }

    @Scheduled(cron = HOURLY)
    void sendPendingEmailNotifications() {
        List<Contact> contactList = contactRepository.findAllByStatus(ContactStatus.PENDING);

        if (contactList.isEmpty()) {
            return;
        }

        sendEmailNotifications(contactList);
    }

    private void sendEmailNotifications(List<Contact> contactList) {
        contactList.forEach(contact -> {
            try {
                log.info("Sending email notification for contact with id: {}", contact.getId());
                emailService.sendEmailNotification(contact);
                contactService.updateContactStatus(contact, ContactStatus.SENT);
            } catch (MessagingException e) {
                log.error("Unable to send an email notification for contact id: {} because of error: {}", contact.getId(), e.getMessage());
            }
        });
    }
}
