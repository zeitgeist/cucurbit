package com.zeitgeist.cucurbit.email;

import com.zeitgeist.cucurbit.contact.Contact;
import lombok.extern.log4j.Log4j2;

import javax.mail.*;

import org.springframework.stereotype.Service;

import javax.mail.internet.*;

import java.util.Properties;

@Service
@Log4j2
public class EmailService {

    private final String CUCURBIT_EMAIL_SENDER_USERNAME;
    private final String CUCURBIT_EMAIL_SENDER_PASSWORD;
    private final EmailMessageFormatter emailMessageFormatter;

    private final Properties prop;

    public EmailService(String CUCURBIT_EMAIL_SENDER_USERNAME,
                        String CUCURBIT_EMAIL_SENDER_PASSWORD,
                        EmailMessageFormatter emailMessageFormatter) {
        this.CUCURBIT_EMAIL_SENDER_USERNAME = CUCURBIT_EMAIL_SENDER_USERNAME;
        this.CUCURBIT_EMAIL_SENDER_PASSWORD = CUCURBIT_EMAIL_SENDER_PASSWORD;
        this.emailMessageFormatter = emailMessageFormatter;

        prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");
    }

    public void sendEmailNotification(Contact contact) throws MessagingException {
        Message message = new MimeMessage(buildSession());
        message.setFrom(new InternetAddress(this.CUCURBIT_EMAIL_SENDER_USERNAME));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(contact.getSendTo()));
        message.setSubject(contact.getSubject());

        String formattedMessage = emailMessageFormatter.formatMessage(contact);
        message.setContent(buildContent(formattedMessage));

        Transport.send(message);
    }

    private Session buildSession() {
        return Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(CUCURBIT_EMAIL_SENDER_USERNAME, CUCURBIT_EMAIL_SENDER_PASSWORD);
            }
        });
    }

    private Multipart buildContent(String message) throws MessagingException {
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setText(message, "utf-8", "html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        return multipart;
    }

}
