package com.zeitgeist.cucurbit.email;

import com.zeitgeist.cucurbit.common.DateUtils;
import com.zeitgeist.cucurbit.contact.Contact;
import org.springframework.stereotype.Service;

@Service
public class EmailMessageFormatter {

    private final DateUtils dateUtils;

    public EmailMessageFormatter(DateUtils dateUtils) {
        this.dateUtils = dateUtils;
    }

    public String formatMessage(Contact contact) {
        String formattedDate = dateUtils.formatDateForEmail(contact.getDateCreated());

        return "<div style='font-family: Arial, sans-serif; font-size: 18px'>" +
                "Date: <strong>" + formattedDate + "</strong> <br/>" +
                "From: <strong>" + contact.getSendFrom() + "</strong> <br/>" +
                "Subject: <strong>" + contact.getSubject() + "</strong> <br/>" +
                "Message: <br/>" + contact.getMessage() +
                "</div>";
    }

}
