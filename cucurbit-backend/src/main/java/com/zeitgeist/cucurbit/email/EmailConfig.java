package com.zeitgeist.cucurbit.email;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailConfig {

    @Value("${cucurbit.email.sender.username}")
    private String emailUsername;

    @Bean(name = "CUCURBIT_EMAIL_SENDER_USERNAME")
    public String getEmailUsername() {
        return emailUsername;
    };

    @Value("${cucurbit.email.sender.password}")
    private String emailPassword;

    @Bean(name = "CUCURBIT_EMAIL_SENDER_PASSWORD")
    public String getEmailPassword() {
        return emailPassword;
    };

    @Value("${cucurbit.email.notification.sendTo}")
    private String emailNotificationRecipient;

    @Bean(name = "CUCURBIT_EMAIL_NOTIFICATION_RECIPIENT")
    public String getEmailNotificationRecipient() {
        return emailNotificationRecipient;
    }
}
