package com.zeitgeist.cucurbit.common.responsehandlers;

public enum CucurbitErrorCode {
	RESOURCE_NOT_FOUND,
	NO_RECIPES_FETCHED
}
