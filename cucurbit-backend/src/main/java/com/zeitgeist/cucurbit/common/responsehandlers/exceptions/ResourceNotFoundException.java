package com.zeitgeist.cucurbit.common.responsehandlers.exceptions;

import com.zeitgeist.cucurbit.common.responsehandlers.CucurbitErrorCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResourceNotFoundException extends Exception {

	private String resourceIdentifier;
	private String resourceName;
	private CucurbitErrorCode errorCode = CucurbitErrorCode.RESOURCE_NOT_FOUND;

	public ResourceNotFoundException(String resourceIdentifier, String resourceName) {
		this.resourceIdentifier = resourceIdentifier;
		this.resourceName = resourceName;
	}

}
