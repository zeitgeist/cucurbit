package com.zeitgeist.cucurbit.common.responsehandlers;

import com.zeitgeist.cucurbit.common.responsehandlers.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = {ResourceNotFoundException.class})
	protected ResponseEntity<CucurbitRestResponseError> handleException(ResourceNotFoundException exception, WebRequest request) {
		StringBuilder errorMessage = new StringBuilder()
				.append("Unable to find ")
				.append(exception.getResourceName())
				.append(" with id ")
				.append(exception.getResourceIdentifier());

		CucurbitRestResponseError cucurbitRestResponseError = new CucurbitRestResponseError();
		cucurbitRestResponseError.setErrorCode(exception.getErrorCode());
		cucurbitRestResponseError.setMessage(errorMessage.toString());

		return new ResponseEntity<>(cucurbitRestResponseError, HttpStatus.NOT_FOUND);
	}
}
