package com.zeitgeist.cucurbit.common.responsehandlers.exceptions;

import com.zeitgeist.cucurbit.common.responsehandlers.CucurbitErrorCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoRecipesFetchedException extends Exception {

    private CucurbitErrorCode errorCode = CucurbitErrorCode.RESOURCE_NOT_FOUND;

}
