package com.zeitgeist.cucurbit.common.responsehandlers;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CucurbitRestResponseError {

	private String message;
	private CucurbitErrorCode errorCode;

}
