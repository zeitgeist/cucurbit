package com.zeitgeist.cucurbit.common;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class DateUtils {

    public LocalDateTime getCurrentDate() {
        return LocalDateTime.now();
    }

    public String formatDateForEmail(LocalDateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM dd, yyyy 'at' hh:mm:ss a");
        return dateTime.format(formatter);
    }

}
