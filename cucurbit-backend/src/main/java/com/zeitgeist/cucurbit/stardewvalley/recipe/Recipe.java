package com.zeitgeist.cucurbit.stardewvalley.recipe;

import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatio;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Recipe {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recipe_sequence")
    @SequenceGenerator(name = "recipe_sequence", sequenceName = "recipe_sequence", allocationSize = 1)
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String imageLink;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private RecipeStatus status;

    @Setter
    @Getter
    @JoinTable(name = "recipe_ingredient_ratio")
    @OneToMany(targetEntity= IngredientRatio.class, fetch=FetchType.EAGER)
    private List<IngredientRatio> ingredients;

    @Getter
    @Setter
    @CreationTimestamp
    private LocalDateTime lastUpdated;

}
