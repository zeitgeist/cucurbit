package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class IngredientRatioService {

    private final IngredientRatioRepository ingredientRatioRepository;

    public IngredientRatioService(IngredientRatioRepository ingredientRatioRepository) {
        this.ingredientRatioRepository = ingredientRatioRepository;
    }

    public void setCurrentIngredientsRatioForDeletion() {
        List<IngredientRatio> existingIngredientsRatio = ingredientRatioRepository.findAllByStatus(IngredientRatioStatus.UPDATED);

        existingIngredientsRatio.forEach(ingredientRatio -> {
            ingredientRatio.setStatus(IngredientRatioStatus.FOR_DELETION);
        });

        ingredientRatioRepository.saveAll(existingIngredientsRatio);
    }

}
