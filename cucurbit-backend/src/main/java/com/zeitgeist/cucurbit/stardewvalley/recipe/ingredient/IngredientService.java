package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class IngredientService {

    private final IngredientRepository ingredientRepository;

    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public void setCurrentIngredientsForDeletion() {
        List<Ingredient> existingIngredients = ingredientRepository.findAllByStatus(IngredientStatus.UPDATED);

        existingIngredients.forEach(ingredient -> {
            ingredient.setStatus(IngredientStatus.FOR_DELETION);
        });

        ingredientRepository.saveAll(existingIngredients);
    }

}
