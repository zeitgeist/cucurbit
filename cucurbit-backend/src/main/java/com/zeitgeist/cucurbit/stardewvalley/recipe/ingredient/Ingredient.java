package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Ingredient {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ingredient_sequence")
    @SequenceGenerator(name = "ingredient_sequence", sequenceName = "ingredient_sequence", allocationSize = 1)
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String imageLink;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private IngredientStatus status;

    @Getter
    @Setter
    @CreationTimestamp
    private LocalDateTime lastUpdated;
}
