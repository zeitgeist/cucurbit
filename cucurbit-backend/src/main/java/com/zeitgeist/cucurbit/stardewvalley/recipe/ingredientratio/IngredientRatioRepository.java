package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngredientRatioRepository extends CrudRepository<IngredientRatio, Long>  {

    List<IngredientRatio> findAllByStatus(IngredientRatioStatus ingredientRatioStatus);
}
