package com.zeitgeist.cucurbit.stardewvalley.recipe;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class RecipeController {

    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeController(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "api/v1/recipe", params = {"page", "size", "name"})
    public Page<Recipe> fetchAllRecipes(
            @RequestParam(name = "page", required = true) Integer page,
            @RequestParam(name = "size", required = true) Integer size,
            @RequestParam(name = "name", required = true) String name
    ) {
        Pageable pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "id");
        return recipeRepository.findAllByNameContainingIgnoreCaseAndStatus(name, RecipeStatus.UPDATED, pageRequest);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "api/v1/recipe", params = {"page", "size"})
    public Page<Recipe> fetchAllRecipes(
            @RequestParam(name = "page", required = true) Integer page,
            @RequestParam(name = "size", required = true) Integer size
    ) {
        Pageable pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "id");
        return recipeRepository.findAllByStatus(RecipeStatus.UPDATED, pageRequest);
    }

}
