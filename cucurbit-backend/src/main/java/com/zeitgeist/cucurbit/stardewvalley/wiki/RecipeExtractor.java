package com.zeitgeist.cucurbit.stardewvalley.wiki;

import com.zeitgeist.cucurbit.stardewvalley.recipe.Recipe;
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.Ingredient;
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatio;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class RecipeExtractor {

    private final String STARDEW_VALLEY_BASE_IMAGE_URL;
    private final IngredientExtractor ingredientExctractor;

    public RecipeExtractor(String STARDEW_VALLEY_WIKI_BASE_URL,
                           IngredientExtractor ingredientExctractor) {
        this.STARDEW_VALLEY_BASE_IMAGE_URL = STARDEW_VALLEY_WIKI_BASE_URL;
        this.ingredientExctractor = ingredientExctractor;
    }

    public List<Recipe> extractRecipesFromHtml(String html) {
        Document document = Jsoup.parse(html);
        List<Recipe> recipes = new ArrayList<>();

        Elements wikiTables = document.select("table.wikitable.sortable");

        wikiTables.forEach(table -> {
            Elements tableHeaders = table.select("th");
            String tableHeadersString = tableHeaders.toString();

            if (tableHeadersString.contains("Ingredients") && tableHeadersString.contains("Recipe Source(s)")) {

                Elements tableRows = table.select("tbody>tr");

                tableRows.forEach(tableRow -> {
                    Elements tableCells = tableRow.select("td .center");
                    if (tableCells.size() > 0) {
                        Recipe recipe = findRecipeDetailsFromRow(tableRow);
                        recipes.add(recipe);
                    }

                });
            }
        });

        return recipes;
    }

    private Recipe findRecipeDetailsFromRow(Element tableRow) {
        Recipe recipe = new Recipe();
        List<IngredientRatio> ingredientList = new ArrayList<>();

        Element imageCell = tableRow.select("td:nth-child(1)").first();
        Element nameCell = tableRow.select("td:nth-child(2)").first();
        Element descriptionCell = tableRow.select("td:nth-child(3)").first();
        Element ingredientsCell = tableRow.select("td:nth-child(4)").first();

        if (imageCell != null) {
            recipe.setImageLink(extractImageLink(imageCell));
        }

        if (nameCell != null) {
            recipe.setName(extractName(nameCell));
        }

        if (descriptionCell != null) {
            recipe.setDescription(descriptionCell.text());
        }

        if (ingredientsCell != null) {

            if (containsAny(ingredientsCell)) {
                Elements ingredientImages = ingredientsCell.select("img");

                for (int i = 0; i< ingredientImages.size() ; i++) {
                    Element ingredientImage = ingredientImages.get(i);
                    Element image = ingredientImage.select("img").first();
                    String name = image.attr("alt").replace(".png", "");
                    String imageLink = STARDEW_VALLEY_BASE_IMAGE_URL + image.attr("src");
                    String quantities = StringUtils.substringBetween(ingredientsCell.text(), "(", ")");

                    Ingredient ingredient = new Ingredient();
                    ingredient.setImageLink(imageLink);
                    ingredient.setName(name);

                    IngredientRatio ingredientRatio = new IngredientRatio();
                    ingredientRatio.setIngredient(ingredient);
                    ingredientRatio.setQuantity(Long.parseLong(quantities));

                    ingredientList.add(ingredientRatio);
                }

            } else {
                Elements ingredientsElement = ingredientsCell.select("span.nametemplate");

                ingredientsElement.forEach(ingredientElement -> {
                    IngredientRatio ingredientRatio = extractIngredientRatioFromNameTemplate(ingredientsElement, ingredientElement);
                    ingredientList.add(ingredientRatio);
                });
            }

            recipe.setIngredients(ingredientList);
        }

        return recipe;
    }

    private boolean containsAny(Element ingredientsCell) {
        return ingredientsCell.text().contains("Any ");
    }

    private IngredientRatio extractIngredientRatioFromNameTemplate(Elements ingredientsElement, Element ingredientElement) {
        Ingredient ingredient = ingredientExctractor.extractIngredientFromNameTemplate(ingredientElement);
        Long quantity = extractIngredientQuantity(ingredientsElement);

        IngredientRatio ingredientRatio = new IngredientRatio();
        ingredientRatio.setIngredient(ingredient);
        ingredientRatio.setQuantity(quantity);

        return ingredientRatio;
    }

    private String extractName(Element nameCell) {
        Element nameElement = nameCell.select("a").first();
        return nameElement.text();
    }

    private String extractImageLink(Element imageCell) {
        Element imageLinkElement = imageCell.select("img").first();
        String imageSource = imageLinkElement.attr("src");
        return STARDEW_VALLEY_BASE_IMAGE_URL + imageSource;
    }

    private Long extractIngredientQuantity(Elements ingredientsElement) {
        String rawIngredientString = ingredientsElement.text();
        String quantityString = rawIngredientString.substring(rawIngredientString.indexOf('(') + 1, rawIngredientString.indexOf(')'));
        return Long.parseLong(quantityString);
    }

}
