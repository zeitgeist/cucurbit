package com.zeitgeist.cucurbit.stardewvalley.wiki;

import com.zeitgeist.cucurbit.common.responsehandlers.exceptions.NoRecipesFetchedException;
import com.zeitgeist.cucurbit.stardewvalley.recipe.Recipe;

import lombok.extern.log4j.Log4j2;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class StardewValleyWikiApi {

    private final RestTemplate restTemplate;
    private final RecipeExtractor recipeTransformer;
    private final String STARDEW_VALLEY_WIKI_COOKING_URL;

    public StardewValleyWikiApi(RestTemplate restTemplate, RecipeExtractor recipeTransformer, String STARDEW_VALLEY_WIKI_COOKING_URL) {
        this.restTemplate = restTemplate;
        this.recipeTransformer = recipeTransformer;
        this.STARDEW_VALLEY_WIKI_COOKING_URL = STARDEW_VALLEY_WIKI_COOKING_URL;
    }

    public List<Recipe> fetchRecipesFromWiki() throws NoRecipesFetchedException {

        UriComponents UriComponents = UriComponentsBuilder
                .fromUriString(STARDEW_VALLEY_WIKI_COOKING_URL)
                .build();

        URI uri = UriComponents.toUri();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>("", headers);

        try {
            ResponseEntity<String> response = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<String>() {
                    }
            );

            return recipeTransformer.extractRecipesFromHtml(response.getBody());
        } catch (HttpStatusCodeException exception) {
            log.error("Failed to fetch recipes from URL: {}.", STARDEW_VALLEY_WIKI_COOKING_URL);
            throw new NoRecipesFetchedException();
        }

    }

}
