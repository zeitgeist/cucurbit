package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {

    public Optional<Ingredient> findByNameAndStatus(String name, IngredientStatus status);
    public List<Ingredient> findAllByStatus(IngredientStatus status);

}
