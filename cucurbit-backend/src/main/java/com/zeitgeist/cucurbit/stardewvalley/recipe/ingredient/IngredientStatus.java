package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient;

public enum IngredientStatus {
    UPDATED,
    FOR_DELETION
}
