package com.zeitgeist.cucurbit.stardewvalley.recipe;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends PagingAndSortingRepository<Recipe, Long> {

    public List<Recipe> findAllByStatus(RecipeStatus status);
    public Page<Recipe> findAllByStatus(RecipeStatus recipeStatus, Pageable pageable);
    public Page<Recipe> findAllByNameContainingIgnoreCaseAndStatus(String name, RecipeStatus recipeStatus, Pageable pageable);

    public Long countByStatus(RecipeStatus recipeStatus);

}
