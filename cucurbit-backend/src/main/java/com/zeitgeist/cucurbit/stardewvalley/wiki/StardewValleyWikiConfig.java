package com.zeitgeist.cucurbit.stardewvalley.wiki;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StardewValleyWikiConfig {

    @Value("${stardewvalley.wiki.cooking.base-url}")
    private String stardewValleyWikiCookingUrl;

    @Bean(name = "STARDEW_VALLEY_WIKI_COOKING_URL")
    public String getStardewValleyWikiCookingUrl() {
        return stardewValleyWikiCookingUrl;
    };

    @Value("${stardewvalley.wiki.base-url}")
    private String stardewValleyWikiBaseUrl;

    @Bean(name = "STARDEW_VALLEY_WIKI_BASE_URL")
    public String getStardewValleyWikiBaseUrl() {
        return stardewValleyWikiBaseUrl;
    };

}
