package com.zeitgeist.cucurbit.stardewvalley.recipe;

import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.Ingredient;
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.IngredientRepository;
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.IngredientStatus;
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatio;
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatioRepository;
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatioStatus;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Log4j2
@Transactional
public class RecipeService {

    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;
    private final IngredientRatioRepository ingredientRatioRepository;

    public RecipeService(RecipeRepository recipeRepository,
                         IngredientRepository ingredientRepository,
                         IngredientRatioRepository ingredientRatioRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
        this.ingredientRatioRepository = ingredientRatioRepository;
    }

    public boolean thereAreUpdatedRecipesPresent() {
        boolean hasRecipesForDeletion = recipeRepository.countByStatus(RecipeStatus.FOR_DELETION) > 0;
        boolean hasUpdatedRecipes = recipeRepository.countByStatus(RecipeStatus.UPDATED) > 0;
        return  hasRecipesForDeletion && hasUpdatedRecipes;
    }

    public void setCurrentRecipesForDeletion() {
        List<Recipe> existingRecipes = recipeRepository.findAllByStatus(RecipeStatus.UPDATED);

        existingRecipes.forEach(recipe -> {
            recipe.setStatus(RecipeStatus.FOR_DELETION);
        });

        recipeRepository.saveAll(existingRecipes);
    }

    public void deleteOldRecipes() {
        List<Recipe> existingRecipes = recipeRepository.findAllByStatus(RecipeStatus.FOR_DELETION);
        recipeRepository.deleteAll(existingRecipes);
    }

    public void saveNewRecipes(List<Recipe> recipes) {
        //TODO: make this less ugly
        for (Recipe recipe : recipes) {
            recipe.setStatus(RecipeStatus.UPDATED);

            for (IngredientRatio ingredientRatio : recipe.getIngredients()) {

                Ingredient ingredient = ingredientRatio.getIngredient();
                Optional<Ingredient> existingIngredient = ingredientRepository.findByNameAndStatus(ingredient.getName(), IngredientStatus.UPDATED);

                if (existingIngredient.isEmpty()) {
                    ingredient.setStatus(IngredientStatus.UPDATED);
                    ingredientRepository.save(ingredient);
                }else{
                    ingredientRatio.setIngredient(existingIngredient.get());
                }

                ingredientRatio.setStatus(IngredientRatioStatus.UPDATED);
                ingredientRatioRepository.save(ingredientRatio);
            }
        }

        recipeRepository.saveAll(recipes);
    }
}
