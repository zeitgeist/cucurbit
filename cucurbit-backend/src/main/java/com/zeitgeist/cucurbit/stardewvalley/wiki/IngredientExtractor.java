package com.zeitgeist.cucurbit.stardewvalley.wiki;

import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.Ingredient;
import org.springframework.stereotype.Service;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@Service
public class IngredientExtractor {

	private final String STARDEW_VALLEY_BASE_IMAGE_URL;

	public IngredientExtractor(String STARDEW_VALLEY_WIKI_BASE_URL) {
		this.STARDEW_VALLEY_BASE_IMAGE_URL = STARDEW_VALLEY_WIKI_BASE_URL;
	}

	public Ingredient extractIngredientFromNameTemplate(Element ingredientElement) {
		Ingredient ingredient = new Ingredient();

		Element imageLinkElement = ingredientElement.select("img").first();
		String imageSource = imageLinkElement.attr("src");
		String imageLink = STARDEW_VALLEY_BASE_IMAGE_URL + imageSource;

		Element nameElement = ingredientElement.select("a").first();
		String name = nameElement.text();

		ingredient.setImageLink(imageLink);
		ingredient.setName(name);

		return ingredient;
	}

}
