package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio;

import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.Ingredient;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class IngredientRatio {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ingredient_ratio_sequence")
    @SequenceGenerator(name = "ingredient_ratio_sequence", sequenceName = "ingredient_ratio_sequence", allocationSize = 1)
    private Long id;

    @OneToOne
    @JoinColumn(name = "ingredient_id")
    @Getter
    @Setter
    private Ingredient ingredient;

    @Getter
    @Setter
    private Long quantity;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private IngredientRatioStatus status;

    @Getter
    @Setter
    @CreationTimestamp
    private LocalDateTime lastUpdated;

}
