package com.zeitgeist.cucurbit.stardewvalley.recipe;

public enum RecipeStatus {
    UPDATED,
    FOR_DELETION
}
