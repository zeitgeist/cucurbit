package com.zeitgeist.cucurbit.stardewvalley.recipe.updater;

import com.zeitgeist.cucurbit.common.responsehandlers.exceptions.NoRecipesFetchedException;
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.IngredientService;
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatioService;
import com.zeitgeist.cucurbit.stardewvalley.recipe.Recipe;
import com.zeitgeist.cucurbit.stardewvalley.recipe.RecipeService;
import com.zeitgeist.cucurbit.stardewvalley.wiki.StardewValleyWikiApi;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Component
@Transactional
public class RecipeUpdaterJob {

    // I'm too lazy to write recipes one by one so this job will create and update the
    // recipes for me. Should the wiki be inaccessible at that time
    // the job will assume that all the recipes are updated, and it will do nothing.

    // FOR DEBUGGING
    // private final static String EVERY_DAY_2AM = "*/10 * * * * *";

    private final static String EVERYDAY_2AM = "0 0 2 * * *";

    private final StardewValleyWikiApi stardewValleyWikiApi;
    private final IngredientService ingredientService;
    private final RecipeService recipeService;
    private final IngredientRatioService ingredientRatioService;

    @Autowired
    public RecipeUpdaterJob(StardewValleyWikiApi stardewValleyWikiApi,
                            IngredientService ingredientService,
                            RecipeService recipeService,
                            IngredientRatioService ingredientRatioService) {
        this.stardewValleyWikiApi = stardewValleyWikiApi;
        this.ingredientService = ingredientService;
        this.recipeService = recipeService;
        this.ingredientRatioService = ingredientRatioService;
    }

    @Scheduled(cron = EVERYDAY_2AM)
    void grabAndSaveLatestRecipes() {

        try {
            log.info("Attempting to update recipes...");

            List<Recipe> recipes = stardewValleyWikiApi.fetchRecipesFromWiki();

            if (recipes.isEmpty()) {
                return;
            }

            ingredientRatioService.setCurrentIngredientsRatioForDeletion();
            recipeService.setCurrentRecipesForDeletion();
            ingredientService.setCurrentIngredientsForDeletion();

            recipeService.saveNewRecipes(recipes);

        } catch (NoRecipesFetchedException e) {
            log.error("Failed to fetch recipes. Nothing will be updated.");
        }

    }
}
