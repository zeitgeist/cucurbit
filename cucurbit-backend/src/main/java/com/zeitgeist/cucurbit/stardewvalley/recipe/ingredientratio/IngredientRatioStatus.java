package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio;

public enum IngredientRatioStatus {
    UPDATED,
    FOR_DELETION
}
