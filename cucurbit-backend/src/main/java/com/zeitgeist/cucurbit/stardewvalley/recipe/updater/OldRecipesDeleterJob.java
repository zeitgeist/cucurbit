package com.zeitgeist.cucurbit.stardewvalley.recipe.updater;

import com.zeitgeist.cucurbit.stardewvalley.recipe.RecipeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Component
@Transactional
public class OldRecipesDeleterJob {

    // FOR DEBUGGING
    // private final static String EVERYDAY_6AM = "*/20 * * * * *";
    private final static String EVERYDAY_6AM = "0 0 6 * * *";

    private final RecipeService recipeService;

    public OldRecipesDeleterJob(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @Scheduled(cron = EVERYDAY_6AM)
    void fetchOldRecipesAndDelete() {

        if (recipeService.thereAreUpdatedRecipesPresent()) {
            log.info("Deleting old recipes...");
            recipeService.deleteOldRecipes();
            return;
        }

        log.info("There are no updated recipes. Old recipes will not be deleted.");
    }
}
