package com.zeitgeist.cucurbit;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.TimeZone;

@Log4j2
@Configuration
public class ApplicationConfig {

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public void setDefaultTimezone() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        log.info("Current time zone set to: {}", TimeZone.getDefault().getDisplayName());
    }

}
