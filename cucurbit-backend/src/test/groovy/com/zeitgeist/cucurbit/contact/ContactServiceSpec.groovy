package com.zeitgeist.cucurbit.contact

import com.zeitgeist.cucurbit.common.DateUtils
import spock.lang.Specification

import java.time.LocalDateTime

class ContactServiceSpec extends Specification {

    ContactService service

    DateUtils dateUtils = Mock()
    ContactRepository contactRepository = Mock()
    String emailRecipient = "recipient@gmail.com"

    void setup() {
        service = new ContactService(dateUtils, contactRepository, emailRecipient)
    }

    void "createAndSaveContact should create and save a contact with the correct values."() {
        given:
        ContactRequestBody requestBody = Mock()
        requestBody.getSendFrom() >> "Cypher@yopmail.com"
        requestBody.getSubject() >> "My Subject"
        requestBody.getMessage() >> "Nothing stays hidden from me. Nothing."

        LocalDateTime dateNow = LocalDateTime.now()
        dateUtils.getCurrentDate() >> dateNow

        when:
        service.createAndSaveContact(requestBody)

        then:
        1 * contactRepository.save(_) >> { Contact actualContactSaved ->
            assert ContactStatus.PENDING == actualContactSaved.getStatus()
            assert dateNow == actualContactSaved.getDateCreated()
            assert requestBody.getSubject() == actualContactSaved.getSubject()
            assert requestBody.getSendFrom() == actualContactSaved.getSendFrom()
            assert emailRecipient == actualContactSaved.getSendTo()
            assert requestBody.getMessage() == actualContactSaved.getMessage()
        }
    }

}
