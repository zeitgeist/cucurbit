package com.zeitgeist.cucurbit.contact

import spock.lang.Specification

class ContactControllerSpec extends Specification {

    ContactController controller
    ContactService contactService = Mock()

    void setup() {
        controller = new ContactController(contactService)
    }

    void "contact should create and save a contact."() {
        given:
        ContactRequestBody requestBody = Mock()

        when:
        controller.contact(requestBody)

        then:
        1 * contactService.createAndSaveContact(requestBody)
    }
}
