package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient

import spock.lang.Specification

class IngredientServiceSpec extends Specification {

    IngredientService service
    IngredientRepository ingredientRepository = Mock()

    void setup() {
        service = new IngredientService(ingredientRepository)
    }

    void "setCurrentIngredientsForDeletion should update status of ingredients to FOR_DELETION"() {
        given:
        Ingredient ingredient = new Ingredient()
        ingredient.setStatus(IngredientStatus.UPDATED)

        List<Ingredient> ingredients = [ ingredient ]

        ingredientRepository.findAllByStatus(IngredientStatus.UPDATED) >> ingredients

        when:
        service.setCurrentIngredientsForDeletion()

        then:
        ingredientRepository.saveAll(_) >> { savedValues ->
            List<Ingredient> savedIngredients = (List<Ingredient>) savedValues.get(0)

            assert 1 == savedIngredients.size()
            assert IngredientStatus.FOR_DELETION == savedIngredients.get(0).getStatus()
        }
    }
}
