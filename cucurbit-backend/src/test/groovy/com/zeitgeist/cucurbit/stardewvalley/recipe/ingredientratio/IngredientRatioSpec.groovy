package com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio


import spock.lang.Specification

class IngredientRatioSpec extends Specification {

    IngredientRatioService service
    IngredientRatioRepository ingredientRatioRepository = Mock()

    void setup() {
        service = new IngredientRatioService(ingredientRatioRepository)
    }

    void "setCurrentIngredientsRatioForDeletion should update status of ingredient ratio to FOR_DELETION"() {
        given:
        IngredientRatio ingredientRatio = new IngredientRatio()
        ingredientRatio.setStatus(IngredientRatioStatus.UPDATED)

        List<IngredientRatio> ingredientRatioList = [ ingredientRatio ]

        ingredientRatioRepository.findAllByStatus(IngredientRatioStatus.UPDATED) >> ingredientRatioList

        when:
        service.setCurrentIngredientsRatioForDeletion()

        then:
        ingredientRatioRepository.saveAll(_) >> { savedValues ->
            List<IngredientRatio> savedIngredientRatios = (List<IngredientRatio>) savedValues.get(0)

            assert 1 == savedIngredientRatios.size()
            assert IngredientRatioStatus.FOR_DELETION == savedIngredientRatios.get(0).getStatus()
        }
    }

}
