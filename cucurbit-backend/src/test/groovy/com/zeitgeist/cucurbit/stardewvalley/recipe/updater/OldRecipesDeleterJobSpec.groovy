package com.zeitgeist.cucurbit.stardewvalley.recipe.updater

import com.zeitgeist.cucurbit.stardewvalley.recipe.RecipeService
import spock.lang.Specification

class OldRecipesDeleterJobSpec extends Specification {

    OldRecipesDeleterJob job
    RecipeService recipeService = Mock()

    void setup() {
        job = new OldRecipesDeleterJob(recipeService)
    }

    void "fetchOldRecipesAndDelete should do nothing if there are no updated recipes."() {
        given:
        recipeService.thereAreUpdatedRecipesPresent() >> false

        when:
        job.fetchOldRecipesAndDelete()

        then:
        0 * recipeService.deleteOldRecipes()
    }

    void "fetchOldRecipesAndDelete should delete old recipes if there are updated recipes present."() {
        given:
        recipeService.thereAreUpdatedRecipesPresent() >> true

        when:
        job.fetchOldRecipesAndDelete()

        then:
        1 * recipeService.deleteOldRecipes()
    }
}
