package com.zeitgeist.cucurbit.stardewvalley.wiki

import com.zeitgeist.cucurbit.common.responsehandlers.exceptions.NoRecipesFetchedException
import com.zeitgeist.cucurbit.stardewvalley.recipe.Recipe
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.HttpStatusCodeException
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponents
import org.springframework.web.util.UriComponentsBuilder
import spock.lang.Specification

class StardewValleyWikiApiSpec extends Specification {

    StardewValleyWikiApi api
    RestTemplate restTemplate = Mock()
    RecipeExtractor recipeTransformer = Mock()
    String STARDEW_VALLEY_WIKI_COOKING_URL = "www.test.com"

    void setup() {
        api = new StardewValleyWikiApi(restTemplate, recipeTransformer, STARDEW_VALLEY_WIKI_COOKING_URL)
    }

    void "fetchRecipesFromWiki should return a NoRecipesFetchedException if the url does not exist."() {
        given:
        UriComponents UriComponents = UriComponentsBuilder
                .fromUriString(STARDEW_VALLEY_WIKI_COOKING_URL)
                .build()

        URI uri = UriComponents.toUri()

        HttpStatusCodeException exception = Mock(HttpStatusCodeException)
        exception.getStatusCode() >> HttpStatus.NOT_FOUND

        restTemplate.exchange(uri, HttpMethod.GET, _, _) >> {throw exception }

        when:
        api.fetchRecipesFromWiki()

        then:
        thrown(NoRecipesFetchedException)
    }

    void "fetchRecipesFromWiki should respond with a list of recipes under normal circumstances."() {
        given:
        UriComponents UriComponents = UriComponentsBuilder
                .fromUriString(STARDEW_VALLEY_WIKI_COOKING_URL)
                .build()

        URI uri = UriComponents.toUri()

        Recipe recipe1 = Mock()
        Recipe recipe2 = Mock()

        List<Recipe> expectedList = [
                recipe1,
                recipe2
        ]

        String responseHtml = "hello world"

        ResponseEntity responseEntity = Mock()
        responseEntity.getBody() >> responseHtml

        restTemplate.exchange(uri, HttpMethod.GET, _, _) >> responseEntity

        recipeTransformer.extractRecipesFromHtml(responseHtml) >> expectedList

        when:
        List<Recipe> actualResponse = api.fetchRecipesFromWiki()

        then:
        expectedList == actualResponse
    }

}
