package com.zeitgeist.cucurbit.stardewvalley.wiki

import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.Ingredient
import spock.lang.Specification
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

class IngredientExtractorSpec extends Specification {


	IngredientExtractor service

	String STARDEW_VALLEY_BASE_IMAGE_URL = "www.imgur.com"

	void setup() {
		service = new IngredientExtractor(STARDEW_VALLEY_BASE_IMAGE_URL)
	}

	void "extractIngredient should return an ingredient with the correct name and image url (normal format)."() {
		given:
		String ingredientHtml = """
			<span class="nametemplate">
			<img alt="Dandelion.png" src="/mediawiki/images/thumb/b/b1/Dandelion.png/24px-Dandelion.png" decoding="async" width="24" 
			height="24" srcset="/mediawiki/images/thumb/b/b1/Dandelion.png/36px-Dandelion.png 1.5x, /mediawiki/images/b/b1/Dandelion.png 2x"> 
			<a href="/Dandelion" title="Dandelion">Dandelion</a>&nbsp;(1)</span>
		"""

		Document document = Jsoup.parse(ingredientHtml)
		Element ingredientElement = document.select("span.nametemplate").first()

		when:
		Ingredient actualIngredient = service.extractIngredientFromNameTemplate(ingredientElement)

		then:
		"Dandelion" == actualIngredient.getName()
		"www.imgur.com/mediawiki/images/thumb/b/b1/Dandelion.png/24px-Dandelion.png" == actualIngredient.getImageLink()
	}


}
