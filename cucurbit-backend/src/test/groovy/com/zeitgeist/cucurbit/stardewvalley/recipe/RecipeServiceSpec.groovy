package com.zeitgeist.cucurbit.stardewvalley.recipe

import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.Ingredient
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.IngredientRepository
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.IngredientStatus
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatio
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatioRepository
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatioStatus
import spock.lang.Specification
import spock.lang.Unroll

class RecipeServiceSpec extends Specification {

    RecipeService service

    RecipeRepository recipeRepository = Mock()
    IngredientRepository ingredientRepository = Mock()
    IngredientRatioRepository ingredientRatioRepository = Mock()

    void setup() {
        service = new RecipeService(recipeRepository, ingredientRepository, ingredientRatioRepository)
    }

    @Unroll
    void "atLeastOneRecipeIsPresent should return #expectedValue if #condition"() {
        given:
        recipeRepository.countByStatus(RecipeStatus.UPDATED) >> existingUpdatedCount
        recipeRepository.countByStatus(RecipeStatus.FOR_DELETION) >> existingForDeletionCount

        when:
        boolean actualResponse = service.thereAreUpdatedRecipesPresent()

        then:
        expectedValue == actualResponse

        where:
        existingUpdatedCount | existingForDeletionCount | expectedValue | condition
        1                    | 1                        | true          | "there are both recipes that are UPDATED and FOR_DELETION."
        0                    | 0                        | false         | "there no recipes that UPDATED and for DELETION."
        0                    | 1                        | false         | "there no recipes that UPDATED and for DELETION."
        1                    | 0                        | false         | "there no recipes that UPDATED and for DELETION."
    }

    void "setCurrentRecipesForDeletion should set 'UPDATED' recipe status to 'FOR_DELETION'"() {
        given:
        Recipe recipe1 = new Recipe()
        recipe1.setStatus(RecipeStatus.UPDATED)

        Recipe recipe2 = new Recipe()
        recipe2.setStatus(RecipeStatus.UPDATED)

        recipeRepository.findAllByStatus(RecipeStatus.UPDATED) >> [
                recipe1,
                recipe2
        ]

        when:
        service.setCurrentRecipesForDeletion()

        then:
        recipeRepository.saveAll(_) >> { savedValues ->
            List<Recipe> savedRecipes = (List<Recipe>) savedValues.get(0)
            assert RecipeStatus.FOR_DELETION == savedRecipes.get(0).getStatus()
            assert RecipeStatus.FOR_DELETION == savedRecipes.get(1).getStatus()
        }
    }

    void "deleteOldRecipes should fetch recipes that have status of FOR_DELETION and delete them."() {
        given:
        Recipe recipe1 = Mock()
        Recipe recipe2 = Mock()

        List<Recipe> recipesForDeletion = [
                recipe1,
                recipe2
        ]

        recipeRepository.findAllByStatus(RecipeStatus.FOR_DELETION) >> recipesForDeletion

        when:
        service.deleteOldRecipes()

        then:
        1 * recipeRepository.deleteAll(recipesForDeletion)
    }

    void "saveNewRecipes should save new recipes along with it's ingredients."() {
        given: "ingredients setup"
        Ingredient fish = new Ingredient()
        fish.setName("Fish")

        Ingredient flour = new Ingredient()
        flour.setName("Flour")

        and: "fish cake setup"
        IngredientRatio ingredientRatio1 = new IngredientRatio()
        ingredientRatio1.setIngredient(fish)
        ingredientRatio1.setQuantity(1L)

        IngredientRatio ingredientRatio2 = new IngredientRatio()
        ingredientRatio2.setIngredient(flour)
        ingredientRatio2.setQuantity(2L)

        List<IngredientRatio> fishCakeIngredients = [
                ingredientRatio1,
                ingredientRatio2
        ]

        Recipe fishCake = new Recipe()
        fishCake.setName("fish cake")
        fishCake.setIngredients(fishCakeIngredients)

        List<Recipe> recipesToSave = [fishCake]

        and:
        ingredientRepository.findByNameAndStatus("Fish", IngredientStatus.UPDATED) >> Optional.empty()
        ingredientRepository.findByNameAndStatus("Flour", IngredientStatus.UPDATED) >> Optional.empty()

        when:
        service.saveNewRecipes(recipesToSave)

        then:
        1 * recipeRepository.saveAll(_) >> { savedValues ->
            List<Recipe> savedRecipes = (List<Recipe>) savedValues.get(0)

            assert 1 == savedRecipes.size()

            Recipe savedFishCake = savedRecipes.get(0)
            assert RecipeStatus.UPDATED == savedFishCake.getStatus()
            assert 2 == savedFishCake.getIngredients().size()

            IngredientRatio savedFishIngredientRatio = savedFishCake.getIngredients().get(0)
            assert  IngredientRatioStatus.UPDATED == savedFishIngredientRatio.getStatus()

            Ingredient savedFish = savedFishIngredientRatio.getIngredient()
            assert IngredientStatus.UPDATED == savedFish.getStatus()
            assert "Fish" == savedFish.getName()

            IngredientRatio savedFlourIngredientRatio = savedFishCake.getIngredients().get(1)
            Ingredient savedFlour = savedFlourIngredientRatio.getIngredient()
            assert IngredientStatus.UPDATED == savedFlour.getStatus()
            assert "Flour" == savedFlour.getName()
        }
    }

    void "saveNewRecipes: If the ingredient already exists, it should use the existing ingredient instead of making a new one."() {
        given: "ingredients setup"
        Ingredient fish = new Ingredient()
        fish.setName("Fish")

        Ingredient existingFish = new Ingredient()
        existingFish.setName("Existing Fish")
        existingFish.setStatus(IngredientStatus.UPDATED)

        and: "fish cake setup"
        IngredientRatio ingredientRatio1 = new IngredientRatio()
        ingredientRatio1.setIngredient(fish)
        ingredientRatio1.setQuantity(1L)

        List<IngredientRatio> fishCakeIngredients = [
                ingredientRatio1
        ]

        Recipe fishCake = new Recipe()
        fishCake.setName("fish cake")
        fishCake.setIngredients(fishCakeIngredients)

        List<Recipe> recipesToSave = [fishCake]

        and:
        ingredientRepository.findByNameAndStatus("Fish", IngredientStatus.UPDATED) >> Optional.of(existingFish)
        ingredientRepository.findByNameAndStatus("Flour", IngredientStatus.UPDATED) >> Optional.empty()

        when:
        service.saveNewRecipes(recipesToSave)

        then:
        1 * recipeRepository.saveAll(_) >> { savedValues ->
            List<Recipe> savedRecipes = (List<Recipe>) savedValues.get(0)

            assert 1 == savedRecipes.size()

            Recipe savedFishCake = savedRecipes.get(0)
            assert RecipeStatus.UPDATED == savedFishCake.getStatus()
            assert 1 == savedFishCake.getIngredients().size()

            IngredientRatio savedFishIngredientRatio = savedFishCake.getIngredients().get(0)
            assert  IngredientRatioStatus.UPDATED == savedFishIngredientRatio.getStatus()

            Ingredient savedFish = savedFishIngredientRatio.getIngredient()
            assert IngredientStatus.UPDATED == savedFish.getStatus()
            assert "Existing Fish" == savedFish.getName()
        }
    }
}
