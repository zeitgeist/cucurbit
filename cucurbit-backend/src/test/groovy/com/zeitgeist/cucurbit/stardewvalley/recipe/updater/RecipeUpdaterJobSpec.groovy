package com.zeitgeist.cucurbit.stardewvalley.recipe.updater

import com.zeitgeist.cucurbit.stardewvalley.recipe.Recipe
import com.zeitgeist.cucurbit.stardewvalley.recipe.RecipeService
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredient.IngredientService
import com.zeitgeist.cucurbit.stardewvalley.recipe.ingredientratio.IngredientRatioService
import com.zeitgeist.cucurbit.stardewvalley.wiki.StardewValleyWikiApi
import spock.lang.Specification

class RecipeUpdaterJobSpec extends Specification {

    RecipeUpdaterJob job

    StardewValleyWikiApi stardewValleyWikiApi = Mock()
    IngredientService ingredientService = Mock()
    RecipeService recipeService = Mock()
    IngredientRatioService ingredientRatioService = Mock()

    void setup() {
        job = new RecipeUpdaterJob(stardewValleyWikiApi, ingredientService, recipeService, ingredientRatioService)
    }

    void "grabAndSaveLatestRecipes should set old recipes for deletion if there new recipes for update."() {
        given:
        Recipe recipe = Mock()
        List<Recipe> recipeList = [ recipe ]
        stardewValleyWikiApi.fetchRecipesFromWiki() >> recipeList

        when:
        job.grabAndSaveLatestRecipes()

        then:
        1 * ingredientRatioService.setCurrentIngredientsRatioForDeletion()
        1 * recipeService.setCurrentRecipesForDeletion()
        1 * ingredientService.setCurrentIngredientsForDeletion()
    }

    void "grabAndSaveLatestRecipes should save new recipes if there are new recipes for update."() {
        given:
        Recipe recipe = Mock()
        List<Recipe> recipeList = [ recipe ]
        stardewValleyWikiApi.fetchRecipesFromWiki() >> recipeList

        when:
        job.grabAndSaveLatestRecipes()

        then:
        recipeService.saveNewRecipes(recipeList)
    }

    void "grabAndSaveLatestRecipes should do nothing if there are no recipes for update"() {
        given:
        stardewValleyWikiApi.fetchRecipesFromWiki() >> []

        when:
        job.grabAndSaveLatestRecipes()

        then:
        0 * recipeService.saveNewRecipes(_)

        and: "it should not set anything for deletion."
        0 * ingredientRatioService.setCurrentIngredientsRatioForDeletion()
        0 * recipeService.setCurrentRecipesForDeletion()
        0 * ingredientService.setCurrentIngredientsForDeletion()
    }

}
