package com.zeitgeist.cucurbit.email

import com.zeitgeist.cucurbit.contact.Contact
import com.zeitgeist.cucurbit.contact.ContactRepository
import com.zeitgeist.cucurbit.contact.ContactService
import com.zeitgeist.cucurbit.contact.ContactStatus
import spock.lang.Specification

class EmailSenderJobSpec extends Specification {

    EmailSenderJob job

    ContactRepository contactRepository = Mock()
    EmailService emailService = Mock()
    ContactService contactService = Mock()

    void setup() {
        job = new EmailSenderJob(contactRepository, emailService, contactService)
    }

    void "sendPendingEmailNotifications should do nothing if there are no PENDING contacts."() {
        given:
        List<Contact> contactList = []

        contactRepository.findAllByStatus(ContactStatus.PENDING) >> contactList

        when:
        job.sendPendingEmailNotifications()

        then:
        0 * emailService.sendEmailNotification(*_)
        0 * contactService.updateContactStatus(_,_)
    }

    void "sendPendingEmailNotifications should send email notifications if the contact list is not empty."() {
        given:
        Contact contact1 = Mock()
        Contact contact2 = Mock()

        List<Contact> contactList = [ contact1, contact2 ]

        contactRepository.findAllByStatus(ContactStatus.PENDING) >> contactList

        when:
        job.sendPendingEmailNotifications()

        then:
        1 * emailService.sendEmailNotification(contact1)
        1 * emailService.sendEmailNotification(contact2)

        and:
        1 * contactService.updateContactStatus(contact1, ContactStatus.SENT)
        1 * contactService.updateContactStatus(contact2, ContactStatus.SENT)
    }
}
