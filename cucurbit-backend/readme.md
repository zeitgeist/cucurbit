----------------------------

### REST Endpoints:

#### GET all recipes
- Returns a paginated list of recipes.
```
GET http://localhost:8080/api/v1/recipe?page={page}&size={size}
```

#### GET all recipes with name
- Returns a paginated list of recipes with the indicated name.
```
GET http://localhost:8080/api/v1/recipe?page={page}&size={size}&name={name}
```

### POST contact
- Sends a contact request.
```
POST http://localhost:8080/api/v1/contact

Request Body: 
{
    "sendFrom" : "Cypher"
    "subject" : "They found my wire."
    "message" : "The back alleys, or the streets of war. There is no difference. Just nicer toys."
}
```