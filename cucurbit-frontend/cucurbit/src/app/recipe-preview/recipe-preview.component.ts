import { Component, Input, OnInit } from "@angular/core";
import { Recipe } from "../backend/stardew-valley/recipe.model";

@Component({
    selector: 'app-recipe-preview',
    templateUrl: './recipe-preview.component.html',
    styleUrls: ['./recipe-preview.component.scss']
  })
  export class RecipePreviewComponent {
    
    @Input() recipe: Recipe | undefined;

  }