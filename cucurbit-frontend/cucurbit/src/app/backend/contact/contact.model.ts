export class Contact {

    public readonly sendFrom: string;
    public readonly subject: string;
    public readonly message: string;

    constructor(sendFrom: string, subject: string, message: string) {
        this.sendFrom = sendFrom;
        this.subject = subject;
        this.message = message;
    }

}