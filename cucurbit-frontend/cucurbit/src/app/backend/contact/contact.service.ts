import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import * as BuildUrl from "build-url";
import { CUCURBIT } from "src/environments/environment";
import { Contact } from "./contact.model";

@Injectable()
export class ContactService {

    private path = 'contact';

    constructor(
		private httpClient: HttpClient
	) {}

    public sendMail(contact: Contact) {
        const fullUrl = BuildUrl(CUCURBIT.BASE_URL, {
			    path: this.path
		    });

        return this.httpClient.post<any>(fullUrl, contact);
    }
}
