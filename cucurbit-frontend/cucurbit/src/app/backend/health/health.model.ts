export class Health {

    public readonly status: string;
    
    constructor(status: string) {
        this.status = status;
    }

}