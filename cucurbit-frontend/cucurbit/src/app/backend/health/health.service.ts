import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import * as BuildUrl from "build-url";
import { Observable } from "rxjs";
import { CUCURBIT } from "src/environments/environment";
import { Health } from "./health.model";

@Injectable()
export class HealthService {

    private path = 'health';

    constructor(
		private httpClient: HttpClient
	) {}

    public performHealthCheck(): Observable<Health> {
        const fullUrl = BuildUrl(CUCURBIT.ACTUATOR_URL, {
			path: this.path
		});

        return this.httpClient.get<Health>(fullUrl);
    }
}
