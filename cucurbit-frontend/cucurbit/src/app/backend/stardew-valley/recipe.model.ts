import { IngredientRatio } from "./ingredient-ratio.model";

export class Recipe {

    public readonly id: number;
    public readonly name: string;
    public readonly description: string;
    public readonly imageLink: string;
    public readonly lastUpdated: Date;
    public readonly ingredients: IngredientRatio[];

    constructor(
        id: number,
        name: string,
        description: string,
        imageLink: string,
        lastUpdated: Date,
        ingredients: IngredientRatio[]
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageLink = imageLink;
        this.lastUpdated = lastUpdated;
        this.ingredients = ingredients;
    }

}