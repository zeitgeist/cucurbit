import { Recipe } from "./recipe.model";

 export class PagedRecipe {
    public readonly totalElements: number;
    public readonly size: number;
    public readonly totalPages: number;
    public readonly numberOfElements: number;
    public readonly number: number;
    public readonly first: boolean;
    public readonly last: boolean;
    public readonly empty: boolean;
    public readonly content: Recipe[];

    constructor(
        totalElements: number,
        size: number,
        totalPages: number,
        numberOfElements: number,
        number: number,
        first: boolean,
        last: boolean,
        empty: boolean,
        content: Recipe[]
    ) {
        this.totalElements = totalElements;
        this.size = size;
        this.numberOfElements = numberOfElements;
        this.totalPages = totalPages;
        this.number = number;
        this.first = first;
        this.last = last;
        this.empty = empty;
        this.content = content;
    }
 }