import { Ingredient } from "./ingredient.model";

export class IngredientRatio {
    public readonly id: number;
    public readonly lastUpdated: Date;
    public readonly quantity: number;
    public readonly ingredient: Ingredient;

    constructor(
        id: number,
        lastUpdated: Date,
        quantity: number,
        ingredient: Ingredient    
    ) {
        this.id = id;
        this.lastUpdated = lastUpdated;
        this.quantity = quantity;
        this.ingredient = ingredient;
    }
}