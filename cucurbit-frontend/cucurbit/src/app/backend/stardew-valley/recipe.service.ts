import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CUCURBIT } from 'src/environments/environment';
import * as BuildUrl from 'build-url';
import { Observable } from 'rxjs';
import { PagedRecipe } from './paged-recipe.model';

@Injectable()
export class RecipeService {

    private path = 'recipe';

	constructor(
		private httpClient: HttpClient
	) {}

	public fetchAllRecipes(page: number, size: number): Observable<PagedRecipe> {
        const queryParams = {
            'page': page.toString(),
            'size': size.toString()
        };

		const fullUrl = BuildUrl(CUCURBIT.BASE_URL, {
			path: this.path,
            queryParams: queryParams
		});

		return this.httpClient.get<PagedRecipe>(fullUrl);
	}

	public searchRecipes(page: number, size: number, name: string): Observable<PagedRecipe> {
		const queryParams = {
            'page': page.toString(),
            'size': size.toString(),
			'name': name
        };

		const fullUrl = BuildUrl(CUCURBIT.BASE_URL, {
			path: this.path,
            queryParams: queryParams
		});

		return this.httpClient.get<PagedRecipe>(fullUrl);
	}

}