export class Ingredient {
    public readonly id: number;
    public readonly name: string;
    public readonly description: string;
    public readonly imageLink: string;
    public readonly lastUpdated: Date;

    constructor(
        id: number,
        name: string,
        description: string,
        imageLink: string,
        lastUpdated: Date
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageLink = imageLink;
        this.lastUpdated = lastUpdated;
    }
}