import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FrontPageComponent } from './front-page/front-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RecipeService } from './backend/stardew-valley/recipe.service';
import { InfoPageComponent } from './info-page/info-page.component';
import { WoodenBorderComponent } from './wooden-border/wooden-border.component';
import { RecipePreviewComponent } from './recipe-preview/recipe-preview.component';
import { AutoFocus } from './autofocus/autofocus.directive';
import { LoadingBarComponent } from './loading-bar/loading-bar.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { ScrollBorderComponent } from './scroll-border/scroll-border.component';
import { ContactService } from './backend/contact/contact.service';
import { HealthService } from './backend/health/health.service';


@NgModule({
  declarations: [
    AppComponent,
    FrontPageComponent,
    InfoPageComponent,
    WoodenBorderComponent,
    RecipePreviewComponent,
    LoadingBarComponent,
    ContactPageComponent,
    ScrollBorderComponent,
    AutoFocus
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    RecipeService,
    ContactService,
    HealthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
