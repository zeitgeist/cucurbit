import { fakeAsync, flush } from "@angular/core/testing";
import { Observable, of } from "rxjs";
import { FrontPageComponent } from "./front-page.component";
import { Recipe } from "../backend/stardew-valley/recipe.model";
import { PagedRecipe } from '../backend/stardew-valley/paged-recipe.model';

describe("FrontPageComponent", () => {

  let component: FrontPageComponent;

  const recipeService = jasmine.createSpyObj(["searchRecipes", "fetchAllRecipes"]);

  beforeEach(async () => {
    component = new FrontPageComponent(recipeService);
  });

  describe("previewRecipe", () => {

    it("should set the recipe to the one currently clicked.", () => {

      const recipe: Recipe = {
        id: 5,
        name: "Fried Rice",
        description: "Some rice fried in oil.",
        imageLink: "www.image.com/friedrice.png",
        lastUpdated: new Date(),
        ingredients: []
      };

      component.previewRecipe(recipe);

      expect(component.recipe).toEqual(recipe);

    });

  });

  describe("performSearch", () => {

    it("should search for the correct recipe.", fakeAsync(() => {

      component.searchForm.controls["recipeName"].setValue("fried rice");

      recipeService.searchRecipes.withArgs(0, 100, "fried rice").and.returnValue(of({}));

      component.performSearch();
      flush();

      expect(recipeService.searchRecipes).toHaveBeenCalledOnceWith(0, 100, "fried rice");

    }));

    it("should display the recipe preview if there is only one result.", fakeAsync(() => {

      component.searchForm.controls["recipeName"].setValue("cake");

      const cake: Recipe = {
        id: 4,
        name: "cake",
        description: "hardened flour.",
        imageLink: "www.image.com/cake.png",
        lastUpdated: new Date(),
        ingredients: []
      };

      const pagedRecipe: PagedRecipe = {
        totalElements: 1,
        size: 1,
        totalPages: 1,
        numberOfElements: 1,
        number: 1,
        first: true,
        last: true,
        empty: false,
        content: [cake]
      };

      recipeService.searchRecipes.withArgs(0, 100, "cake").and.returnValue(of(pagedRecipe));

      component.performSearch();
      flush();

      expect(component.recipe).toEqual(cake);

    }));

    it("should not display the recipe preview if there are multiple results.", fakeAsync(() => {

      component.searchForm.controls["recipeName"].setValue("cake");

      const cake: Recipe = {
        id: 2,
        name: "cake",
        description: "hardened flour.",
        imageLink: "www.image.com/cake.png",
        lastUpdated: new Date(),
        ingredients: []
      };

      const riceCake: Recipe = {
        id: 1,
        name: "riceCake",
        description: "hardened rice flour.",
        imageLink: "www.image.com/rice_cake.png",
        lastUpdated: new Date(),
        ingredients: []
      };

      const pagedRecipes: PagedRecipe = {
        totalElements: 2,
        size: 2,
        totalPages: 1,
        numberOfElements: 2,
        number: 1,
        first: true,
        last: true,
        empty: false,
        content: [cake, riceCake]
      };

      recipeService.searchRecipes.withArgs(0, 100, "cake").and.returnValue(of(pagedRecipes));

      component.performSearch();
      flush();

      expect(component.recipe).toBeUndefined();

    }));

    it("should display multiple recipes if there are multiple results.", fakeAsync(() => {

      component.searchForm.controls["recipeName"].setValue("cake");

      const cake: Recipe = {
        id: 2,
        name: "cake",
        description: "hardened flour.",
        imageLink: "www.image.com/cake.png",
        lastUpdated: new Date(),
        ingredients: []
      };

      const riceCake: Recipe = {
        id: 1,
        name: "riceCake",
        description: "hardened rice flour.",
        imageLink: "www.image.com/rice_cake.png",
        lastUpdated: new Date(),
        ingredients: []
      };

      const pagedRecipes: PagedRecipe = {
        totalElements: 2,
        size: 2,
        totalPages: 1,
        numberOfElements: 2,
        number: 1,
        first: true,
        last: true,
        empty: false,
        content: [cake, riceCake]
      };

      recipeService.searchRecipes.withArgs(0, 100, "cake").and.returnValue(of(pagedRecipes));

      component.performSearch();
      flush();

      expect(component.recipeList).toEqual(pagedRecipes.content);

    }));

  });

  describe("ngOnInit", () => {
    it("should fetch all the recipes on load.", fakeAsync(() => {

      const riceCake: Recipe = {
        id: 1,
        name: "riceCake",
        description: "hardened rice flour.",
        imageLink: "www.image.com/rice_cake.png",
        lastUpdated: new Date(),
        ingredients: []
      };

      const pagedRecipes: PagedRecipe = {
        totalElements: 1,
        size: 1,
        totalPages: 1,
        numberOfElements: 1,
        number: 1,
        first: true,
        last: true,
        empty: false,
        content: [riceCake]
      };

      recipeService.fetchAllRecipes.withArgs(0, 100).and.returnValue(of(pagedRecipes));

      component.ngOnInit();
      flush();

      expect(component.recipeList).toEqual(pagedRecipes.content);

    }));
  });

});
