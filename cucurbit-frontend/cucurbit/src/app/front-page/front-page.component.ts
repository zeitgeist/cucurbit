import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Subject, Subscription } from "rxjs";
import { PagedRecipe } from "../backend/stardew-valley/paged-recipe.model";
import { Recipe } from "../backend/stardew-valley/recipe.model";
import { RecipeService } from "../backend/stardew-valley/recipe.service";

@Component({
  selector: "app-front-page",
  templateUrl: "./front-page.component.html",
  styleUrls: ["./front-page.component.scss"]
})
export class FrontPageComponent implements OnInit {

  public recipe: Recipe | undefined;
  public isPageReady = false;
  public isPageFirstLoading = true;
  public recipeList: Recipe[];
  public searchForm = new FormGroup({
    recipeName: new FormControl("")
  });

  constructor(private recipeService: RecipeService) {
    this.recipeList = [];
  }

  ngOnInit(): void {
    this.recipeService.fetchAllRecipes(0, 100).subscribe(response => {
      this.loadRecipes(response);
      this.isPageFirstLoading = false;
    });
  }

  // TODO: Add debounce.
  public performSearch():void {
    const recipeName = this.searchForm.get("recipeName")?.value;

    this.recipeService.searchRecipes(0, 100, recipeName)
    .subscribe(response => {
      this.loadSearchResults(response);
    });
  }

  public previewRecipe(recipe: Recipe):void {
    this.recipe = recipe;
  }

  private loadSearchResults(response: PagedRecipe) {
    this.isPageReady = false;
    this.recipe = undefined;

    if(response.numberOfElements == 1) {
      this.loadRecipe(response.content[0]);
    }

    this.loadRecipes(response);
  }

  private loadRecipe(recipe: Recipe) {
    this.recipe = recipe;
  }

  private loadRecipes(response: PagedRecipe) {
    this.clearRecipes();
    this.recipeList = response.content;
    this.isPageReady = true;
  }

  private clearRecipes() {
    this.recipeList = [];
  }

}
