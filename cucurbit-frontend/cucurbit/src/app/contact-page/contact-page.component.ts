import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Contact } from "../backend/contact/contact.model";
import { ContactService } from "../backend/contact/contact.service";
import { Health } from "../backend/health/health.model";
import { HealthService } from "../backend/health/health.service";


@Component({
    selector: "app-contact-page",
    templateUrl: "./contact-page.component.html",
    styleUrls: ["./contact-page.component.scss"]
  })
export class ContactPageComponent implements OnInit {

    public isPageReady: boolean = false;

    public contactForm = new FormGroup({
        sendFrom: new FormControl("", Validators.required),
        subject: new FormControl(""),
        message: new FormControl("", Validators.required)
    });

    constructor(
      public contactService: ContactService,
      public healthService: HealthService  
    ) {}
    
    ngOnInit(): void {
      this.healthService.performHealthCheck()
      .subscribe((response: Health) => {
        if(response.status === "UP") {
          this.isPageReady = true;
        }
      });
    }

    public isInvalid(field: string, validator: string) {
      return this.contactForm?.dirty && this.contactForm?.controls[field]?.errors?.[validator];
    }

    public submitContactForm(): void {
      console.log(this.contactForm)
      if (this.contactForm.invalid) {
        return;
      }

      const sendFrom = this.contactForm.get("sendFrom")?.value;
      const subject = this.contactForm.get("subject")?.value;
      const message = this.contactForm.get("message")?.value;

      const contact = new Contact(sendFrom, subject, message);

      this.contactService.sendMail(contact).subscribe(() => {
          this.contactForm.reset();
      });
    }

}
