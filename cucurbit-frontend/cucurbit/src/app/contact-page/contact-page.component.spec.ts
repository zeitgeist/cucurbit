import { fakeAsync, flush } from "@angular/core/testing";
import { ContactPageComponent } from "./contact-page.component";
import { of } from "rxjs";
import { ContactService } from '../backend/contact/contact.service';
import { Health } from "../backend/health/health.model";

describe("ContactPageComponent", () => {

  let component: ContactPageComponent;

  let contactService: any;
  let healthService: any;

  beforeEach(async () => {
    contactService = jasmine.createSpyObj(["sendMail"]);
    healthService = jasmine.createSpyObj(["performHealthCheck"]);

    component = new ContactPageComponent(contactService, healthService);

    component.contactForm.reset();
  });

  describe("ngOnInit", () => {

    it("should make the page ready if the backend is up and running.", fakeAsync(() => {

      const response = new Health("UP");

      healthService.performHealthCheck.and.returnValue(of(response));

      component.ngOnInit();

      expect(component.isPageReady).toBeTrue();

    }));

  });

  describe("submitContactForm", () => {

    it("should not do anything if the form is invalid (message is blank).", () => {


      component.contactForm.controls["sendFrom"].setValue("Cypher");
      component.contactForm.controls["subject"].setValue("Oh, this is a nice spot!");
      component.contactForm.controls["message"].setValue("");

      component.submitContactForm();

      expect(contactService.sendMail).toHaveBeenCalledTimes(0);

    });

    it("should not do anything if the form is invalid (sendFrom is blank).", () => {

      component.contactForm.controls["sendFrom"].setValue("");
      component.contactForm.controls["subject"].setValue("My camera is destroyed.");
      component.contactForm.controls["message"].setValue("Ah, my eyes are down.");

      component.submitContactForm();

      expect(contactService.sendMail).toHaveBeenCalledTimes(0);

    });

    it("should not do anything if the form is invalid (both sendFrom and message is blank).", () => {

      component.contactForm.controls["sendFrom"].setValue("");
      component.contactForm.controls["subject"].setValue("My camera is destroyed.");
      component.contactForm.controls["message"].setValue("");

      component.submitContactForm();

      expect(contactService.sendMail).toHaveBeenCalledTimes(0);

    });

    it("should send the mail if the form is valid.", fakeAsync(() => {

      component.contactForm.controls["sendFrom"].setValue("Cypher");
      component.contactForm.controls["subject"].setValue("My camera is destroyed.");
      component.contactForm.controls["message"].setValue("Ah, my eyes are down.");

      contactService.sendMail.withArgs(jasmine.anything()).and.returnValue(of({}));

      component.submitContactForm();
      flush();

      expect(contactService.sendMail).toHaveBeenCalledTimes(1);

    }));


  });

});
