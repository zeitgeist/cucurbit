export class Artist {

    public readonly name: string;
    public readonly sourceUrl: string;
    public readonly workTitle: string;

    constructor(
        name: string,
        sourceUrl: string,
        workTitle: string,
    ){
        this.name = name;
        this.sourceUrl = sourceUrl;
        this.workTitle = workTitle;
    }
}