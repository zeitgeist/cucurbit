import { Component, OnInit } from "@angular/core";
import { Artist } from "./artist.model";

@Component({
    selector: 'app-info-page',
    templateUrl: './info-page.component.html',
    styleUrls: ['./info-page.component.scss']
  })
  export class InfoPageComponent implements OnInit {

    public artistList: Artist[] = [];

    ngOnInit(): void {
      this.populateArtists();
    }

    private populateArtists() {
      this.artistList = [
        new Artist("Eliot Truelove", "https://www.whatfontis.com/FF_Stardew-Valley-Regular.font", "Stardew Valley Regular font"),
        new Artist("Frog_Lilly","http://www.rw-designer.com/cursor-set/stardew-valley","Stardew Valley Cursor"),
        new Artist("Cowsplay","https://www.reddit.com/r/StardewValley/comments/4dtgp7/by_popular_request_a_stardew_valley_font_for_your","Stardew Valley Font"),
        new Artist("CommonEvils","https://www.deviantart.com/commonevils/art/Common-s-Stardew-Valley-OC-Template-784825741","Common's Stardew Valley OC Template"),
        new Artist("Eric Barone (@ConcernedApe)", "https://twitter.com/concernedape", "Stardew Valley")
      ];
    }
  }
