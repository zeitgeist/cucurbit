export const environment = {
  production: true
};

export const CUCURBIT = {
  BASE_URL: 'https://cucurbit-backend.herokuapp.com/api/v1',
  ACTUATOR_URL: 'https://cucurbit-backend.herokuapp.com/actuator'
};