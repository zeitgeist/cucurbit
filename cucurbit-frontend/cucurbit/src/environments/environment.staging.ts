export const environment = {
  production: false
};

export const CUCURBIT = {
  BASE_URL: 'https://cucurbit-backend-staging.herokuapp.com/api/v1',
  ACTUATOR_URL: 'https://cucurbit-backend-staging.herokuapp.com/actuator'
};